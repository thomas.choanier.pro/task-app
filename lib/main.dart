import 'package:flutter/cupertino.dart';
import 'package:task_app/app.dart';

Future<void> main() async {
  runApp(const App());
}
