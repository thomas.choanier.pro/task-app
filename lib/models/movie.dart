import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'movie.g.dart';

@JsonSerializable()
class Movie extends Equatable {
  final String title;
  final String overview;
  final double vote;
  final int id;

  const Movie({
    required this.title,
    this.overview = '',
    this.vote = 0.0,
    required this.id,
  });

  @override
  List<Object> get props => [
        title,
        overview,
        vote,
        id,
      ];

  factory Movie.fromJson(Map<String, dynamic> json) => _$MovieFromJson(json);

  Map<String, dynamic> toJson() => _$MovieToJson(this);
}
